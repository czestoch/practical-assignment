// Author: Justyna Anna Czestochowska

import org.apache.log4j.Logger
import org.apache.log4j.Level
import org.apache.spark.sql._
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.rdd._


object Assignment3 {
//------------------------------------------------------------------------------
    Logger.getLogger("org").setLevel(Level.ERROR)
    Logger.getLogger("akka").setLevel(Level.ERROR)
    val spark = SparkSession.builder
                            .appName("SparkSQL").getOrCreate()

//------------------------------------------------------------------------------
    def main(args: Array[String]) = {
        run(loadData()).show(50)
        spark.stop()
    }

//------------------------------------------------------------------------------
    case class Tag(tagId: Int, tag: String)
    def run(collections: (DataFrame, DataFrame, DataFrame, DataFrame))
                                                              : Dataset[Tag] = {
        import spark.implicits._
        val (genomes, tags, links, imdb) = collections
        val joint = links.withColumn("tconst", format_string("tt%07d", col("imdbId")))
                    .join(imdb, "tconst")
        val worstMovies = joint.filter(col("numVotes") >= 150)
                               .sort(asc("averageRating"))
                               .select("movieId").limit(2000).cache
        val window = Window.partitionBy("movieId").orderBy(desc("relevance"))
        val mostRelevant = worstMovies.join(genomes, "movieId")
                                      .withColumn("rn", row_number.over(window))
                                      .where(col("rn") <= 200)
        val mostFrequent = mostRelevant.groupBy("tagId")
                                       .agg(count("tagId").as("count"))
                                       .sort(desc("count"))
        mostFrequent.join(tags, "tagId")
                    .sort(desc("count"))
                    .select("tagId", "tag").as[Tag]
    }

//------------------------------------------------------------------------------
//  Auxiliary
//------------------------------------------------------------------------------
    def loadData() = {
        import spark.implicits._
        val imdbDir = "/cs449/imdb/"
        val mlensDir = "/cs449/movielens/ml-latest/"

        // Files
        val genscoresFile = mlensDir + "genome-scores.csv"
        val gentagsFile = mlensDir + "genome-tags.csv"
        val linksFile = mlensDir + "links.csv"
        val imdbratingsFile = imdbDir + "title.ratings.tsv"

        val opts = Map("header" -> "true", "inferSchema" -> "true")
        val optsTab = opts + ("sep" -> "\t")

        (
            spark.read.options(opts).csv(genscoresFile).as("GenomeScores"),
            spark.read.options(opts).csv(gentagsFile).as("GenomeTags"),
            spark.read.options(opts).csv(linksFile).as("MLensLinks"),
            spark.read.options(optsTab).csv(imdbratingsFile).as("ImdbRatings")
        )
    }
//------------------------------------------------------------------------------
}
