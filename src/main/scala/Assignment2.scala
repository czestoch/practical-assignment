// Author: Justyna Anna Czestochowska

import org.apache.log4j.Logger
import org.apache.log4j.Level
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.rdd._


object Assignment2 {
//------------------------------------------------------------------------------
// Main
//------------------------------------------------------------------------------
    def main(args: Array[String]) = {
        Logger.getLogger("org").setLevel(Level.ERROR)
        Logger.getLogger("akka").setLevel(Level.ERROR)

        val spark = SparkSession.builder
                                 .appName("DBLP").getOrCreate()
        val result = run(loadData(spark))
        result.take(20).foreach(println)
        spark.stop()
    }

//------------------------------------------------------------------------------
    case class MapInput(year: Long, authors: List[String])
    type AuthorPairCount = ((String, String), Int)
    def run(publications: RDD[MapInput]): RDD[AuthorPairCount] =
        mapReduce(publications, mapper, reducer)

//------------------------------------------------------------------------------
//  MapReduce
//------------------------------------------------------------------------------
    def mapReduce(publications: RDD[MapInput],
                  map: MapInput => List[AuthorPairCount],
                  reduce: (Int, Int) => Int): RDD[AuthorPairCount] =
        publications.filter(row => row.year > 2000)
                    .flatMap(pub => map(pub))
                    .reduceByKey(reduce).sortBy(-_._2)

//------------------------------------------------------------------------------
    def mapper(pub: MapInput): List[AuthorPairCount] = {
        val authorsCombinations: List[List[String]] = pub.authors.combinations(2).toList
        authorsCombinations.map(authors => {
          if (authors.head < authors(1)) {
            new AuthorPairCount((authors.head, authors(1)), 1)
          }
          else {
            new AuthorPairCount((authors(1), authors.head), 1)
          }
        })
    }

////------------------------------------------------------------------------------
    def reducer(count1: Int, count2: Int): Int = count1 + count2

//------------------------------------------------------------------------------
//  Auxiliary
//------------------------------------------------------------------------------
    def loadData(spark: SparkSession) = {
        import spark.implicits._
        val datafile = "/cs449/dblp/publications.json"
        spark.read
             .json(datafile)
             .filter(col("year").isNotNull && col("authors").isNotNull)
             .select("year", "authors")
             .as[MapInput].rdd
    }
//------------------------------------------------------------------------------
}
