// Author: Justyna Anna Czestochowska

import org.apache.log4j.Logger
import org.apache.log4j.Level
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.rdd._


object Assignment1 {
//------------------------------------------------------------------------------
    def main(args: Array[String]) = {
        Logger.getLogger("org").setLevel(Level.ERROR)
        Logger.getLogger("akka").setLevel(Level.ERROR)

        val spark = SparkSession.builder
                                .appName("KNN").getOrCreate()
        val result = find10Closest(185544, loadData(spark))
        result.foreach(println)
        spark.stop()
    }

//------------------------------------------------------------------------------
    case class Rating(userId: Int, movieId: Int, rating: Double, timestamp: Int)
    def find10Closest(user: Int,
                      ratings: RDD[Rating]): Array[(Int, Double)] = {
      val usersMovies: RDD[(Int, Iterable[Int])] = ratings.groupBy(rating => rating.userId)
                                                          .map(pair => (pair._1,
                                                                        pair._2
                                                                          .filter(rating => rating.rating > 3)
                                                                          .map(rating => rating.movieId)))
                                                          .filter(pair => pair._2.nonEmpty).cache()

      val queryUserMovies: Set[Int] = ratings.filter(rating => rating.userId == user && rating.rating > 3)
                                             .map(rating => rating.movieId).collect().toSet


      val distances: RDD[(Int, Double)] = usersMovies.map(userMoviesPair =>
          (userMoviesPair._1, jaccard(userMoviesPair._2.toSet, queryUserMovies))
      )
      distances.sortBy(-_._2).take(11).tail
    }

//------------------------------------------------------------------------------
    def jaccard(a: Set[Int], b: Set[Int]): Double = {
        a.intersect(b).size.toDouble / a.union(b).size.toDouble
    }

//------------------------------------------------------------------------------
//  Auxiliary
//------------------------------------------------------------------------------
    def loadData(spark: SparkSession) = {
        import spark.implicits._
        val datafile = "/cs449/movielens/ml-latest/ratings.csv"
        spark.read.options(Map("header" -> "true", "inferSchema" -> "true"))
             .csv(datafile)
             .as[Rating].rdd
    }
//------------------------------------------------------------------------------
}
